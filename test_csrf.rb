require "rubygems"
require "rspec"
require "watir-webdriver"
require "net/http"

describe "testing headers" do

  let(:browser) { @browser ||= Watir::Browser.new :chrome }
  before { browser.goto "http://localhost:3000" }
  after { browser.close }

  it "should register and create a project" do
    browser.goto "http://localhost:3000/users/sign_up"
    browser.text_field(:name => "user[email]").set "mkonda-testa@jemurai.com"
    browser.text_field(:name => "user[password]").set "password"
    browser.text_field(:name => "user[password_confirmation]").set "password"
    sleep(3)
    browser.button(:name => "commit").click
    sleep(3)

    # Create a project then ... 

    # Get teh url set here ...

    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    request = Net::HTTP::Post.new(uri.request_uri)
    request['Cookie'] = cookies
    request.set_form_data( {
      "project[verified]" => true,
      "_method" => "put",
      "authenticity_token" => "Yztc%2FC5AnlbiJW8XWqb5mhJrA9TB%2BkzYTJ7EsDu1hRc%3D", # THIS IS THE WRONG TOKEN!!!
      "project[name]"=> "csrf attempt",
      "project[description]" => "csrf attempt",
      "project[priority]" => "2",
      "project[tier]" => "3",
      "project[rich_description]"=>"hi",
      "commit"=>"Update Project" })
    response = http.request(request)
    @csrf_response = response.body
  end

end
