require "rubygems"
require "rspec"
require "watir-webdriver"
require "net/http"

describe "testing headers" do

  let(:browser) { @browser ||= Watir::Browser.new :chrome }
  before { browser.goto "http://localhost:3000" }
  after { browser.close }

  it "should register and create a project" do
    browser.goto "http://localhost:3000/users/sign_up"
    browser.text_field(:name => "user[email]").set "mkonda-testa@jemurai.com"
    browser.text_field(:name => "user[password]").set "password"
    browser.text_field(:name => "user[password_confirmation]").set "password"
    sleep(3)
    browser.button(:name => "commit").click
    sleep(3)

    # Create a project then ...

    Given(/^a new project created by a user$/) do
  uuid = SecureRandom.uuid
  @user1 = "fb_user_1_#{uuid}@jemurai.com"
  register_as_user(@user1, "password")
#  logout(@user1)
#  login_as_user(@user1, 'password')
  new_project("Insecure Deirect Object Reference #{uuid}", "Forceful Browsing Desc")
  @url = current_url
end

When(/^a different person attempts to access the project$/) do
  logout(@user1)
  uuid = SecureRandom.uuid
  @user2 = "fb_user_2_#{uuid}@jemurai.com"
  register_as_user(@user2, "password")
#  logout(@user2)
#  login_as_user(@user2, 'password')
end

Then(/^the system should prevent access$/) do
  visit @url
  expect(page).not_to have_content "Forceful Browsing Desc"
end

end
