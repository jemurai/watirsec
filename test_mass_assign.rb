require "rubygems"
require "rspec"
require "watir-webdriver"
require "net/http"

describe "testing headers" do

  let(:browser) { @browser ||= Watir::Browser.new :chrome }
  before { browser.goto "http://localhost:3000" }
  after { browser.close }

  it "should register and create a project" do
    browser.goto "http://localhost:3000/users/sign_up"
    browser.text_field(:name => "user[email]").set "mkonda-testa@jemurai.com"
    browser.text_field(:name => "user[password]").set "password"
    browser.text_field(:name => "user[password_confirmation]").set "password"
    sleep(3)
    browser.button(:name => "commit").click
    sleep(3)

    # Create a project then ...

    # Get teh url set here ...
    click_link 'Edit'
    url = current_url
    cookies = Capybara.current_session.driver.browser.manage.all_cookies
    csrf_token = Capybara.current_session.driver.browser.find_element(:xpath, "//meta[@name='csrf-token']").attribute('content');

    cookie = cookies[0]
    detail = cookie[:value]
    url = url.gsub( /\/edit\z/, '')  # Post to root, not the /edit

    # Switch mode to net::http
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    request = Net::HTTP::Post.new(uri.request_uri)
    request['Cookie'] = cookies
    request['_triage_session'] = detail
    request.set_form_data( {
        "project[verified]" => true,   # This is the problem right here.
        "_method" => "put",
        "authenticity_token" => "#{csrf_token}",
        "project[name]"=> "mass assigned",
        "project[description]" => "mass assigned",
        "project[priority]" => "2",
        "project[tier]" => "3",
        "project[rich_description]"=>"hi",
        "commit"=>"Update Project" })
    @response = http.request(request)

    # TODO:  How to tell the response is correct.
  end

end
