# README #

This is a simple repo of Watir scripts for test automation.

### What is this repository for? ###

* Test different security scenarios with Watir

Thus far, supported scenarios included:
* Testing headers are being set correctly.
* Testing a specific mass assignment scenario.
* Testing CSRF scenario.


### How do I get set up? ###

* gem install watir
* gem install rspec
* gem install watir-webdriver
* rspec test_spec.rb (or whatever tests you want to run)

### Who do I talk to? ###

* Matt Konda or Maurice Rabb

### Reference ###

https://watir.com/
